AWS VPC Peering
===============
This role is part of the [Mirabeau Cloud Framework](https://gitlab.com/mirabeau/cloud-framework/)

Creates Cloudformation stack for VPC Peering by specifying the VPC environment abbreviations to peer with.
For each peering, ACLs and Routes will be created as well.

Peering only works with VPC's rolled out with the MCF's aws-vpc role.

A test playbook has been supplied which creates 2 VPCs and then peers them together.
You can run this playbook using the following command:
```bash
ansible-playbook aws-vpc-peering/tests/test.yml --inventory aws-vpc-peering/tests/inventory.yml
```
This command should run outside of the role dir and requires the aws-vpc role to be in the same root dir as well.

Requirements
------------
Ansible version 2.5.4 or higher
Python 2.7.x

Required python modules:
* boto
* boto3
* awscli

Dependencies
------------
* aws-vpc

Role Variables
--------------
### _General_
The following params should be available for Ansible during the rollout of this role:
```yaml
aws_region      : <aws region, eg: eu-west-1>
owner           : <owner, eg: mirabeau>
account_name    : <aws account name>
account_abbr    : <aws account generic environment>
environment_type: <environment>
environment_abbr: <environment abbriviation>
```

Role Defaults
--------------
```yaml
cloudformation_tags   : {}
tag_prefix            : "mcf"
aws_vpc_peering_params:
  debug: False
  peers: []
```

Example Playbooks
----------------
Create peering connection between a tools VPC and the dev,tst,acc VPCs.
```yaml
---
- hosts: localhost
  connection: local
  gather_facts: False
  roles:
    # Create a testing VPC to peer with later
    - role: aws-vpc
      vars:
        environment_type: "test"
        environment_abbr: "tst"
        aws_vpc_params:
          debug                 : True
          network               : "10.129.2.0/24"
          create_nat_gateway    : False
          public_subnet_weight  : 0
          private_subnet_weight : 1
          database_subnet_weight: 0
          domain_names          : "{{ environment_abbr }}.{{ owner }}.cloud"
    # Create the tools VPC itself
    - role: aws-vpc
      vars:
        environment_type: "tools"
        environment_abbr: "tool"
        aws_vpc_params:
          debug                 : True
          network               : "10.129.1.0/24"
          create_nat_gateway    : False
          public_subnet_weight  : 0
          private_subnet_weight : 1
          database_subnet_weight: 0
          domain_names          : "{{ environment_abbr }}.{{ owner }}.cloud"
  vars:
    tag_prefix      : "mcf"
    aws_region      : "eu-west-1"
    owner           : "myself"
    account_name    : "my-dta"
    environment_type: "tools"
    environment_abbr: "tool"
    aws_vpc_peering_params:
      debug: True
      peers:
        - environment_abbr: tst
          region: eu-west-1
  tasks:
    # Now create the VPC peer between the tools VPC and test VPC
    - include_role:
        name: aws-vpc-peering
```

License
-------
GPLv3

Author Information
------------------
Rob Reus <rreus@mirabeau.nl>
